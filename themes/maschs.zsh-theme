autoload -U colors && colors
PROMPT='%{$fg[magenta]%} λ ' # λ
# RPS1='%{$fg[blue]%}%~%{$reset_color%} '
RPS1='$(vi_mode_prompt_info) %{$fg[white]%}%2~$(git_prompt_info) %{$fg_bold[blue]%}%n%{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg[yellow]%}("
ZSH_THEME_GIT_PROMPT_SUFFIX=")%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%} ⚡%{$fg[yellow]%}"
